package com.susp.passwords.generator;

import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

class PasswordWindow extends StackPane {
    private CheckBox digitsFlag;
    private CheckBox uppersFlag;
    private CheckBox lowersFlag;
    private CheckBox punctuationsFlag;

    private Spinner<Integer> symbolsQuantity;
    private Spinner<Integer> passwordsQuantity;

    private Label symbolsLabel;
    private Label passwordsLabel;

    private Button generatePasswords;
    private ListView<String> passwordsList;

    private void init() {
        uppersFlag = createCheckBox("Use upper symbols");
        lowersFlag = createCheckBox("Use lower symbols");

        digitsFlag = createCheckBox("Use digits");
        punctuationsFlag = createCheckBox("Use punctuation symbols");

        symbolsLabel = new Label("Symbols quantity");
        symbolsQuantity = createSpinner(1, 100, 8);

        passwordsLabel = new Label("Passwords quantity");
        passwordsQuantity = createSpinner(1, 100, 1);
        passwordsQuantity.valueProperty().addListener(
                (observable, oldValue, newValue) -> setPasswordsButtonText(newValue)
        );

        generatePasswords = new Button("Generate password");
        generatePasswords.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            switch (event.getCode()) {
                case SPACE:
                case ENTER: {
                    generatePasswords();
                    break;
                }
            }
        });
        generatePasswords.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (Objects.equals(event.getButton(), MouseButton.PRIMARY)) {
                generatePasswords();
            }
        });

        passwordsList = new ListView<>();
        passwordsList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        passwordsList.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            KeyCodeCombination codeCombination = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_ANY);
            if (Objects.equals(codeCombination.getCode(), event.getCode())) {
                ObservableList<String> posList = passwordsList.getSelectionModel().getSelectedItems();
                final ClipboardContent content = new ClipboardContent();
                StringBuilder stringBuilder = new StringBuilder();
                for (String pos : posList) {
                    stringBuilder.append(pos);
                    if (posList.indexOf(pos) != posList.size() - 1) {
                        stringBuilder.append("\n");
                    }
                }
                content.putString(stringBuilder.toString());
                Clipboard.getSystemClipboard().setContent(content);
            }
        });
    }

    PasswordWindow() {
        init();

        VBox symbolsAndLabel = new VBox(5);
        symbolsAndLabel.setPadding(new Insets(5, 5, 5, 5));
        ObservableList<Node> symbolsAndLabelChildrens = symbolsAndLabel.getChildren();
        symbolsAndLabelChildrens.addAll(symbolsLabel, symbolsQuantity);

        VBox passwordsAndLabel = new VBox(5);
        passwordsAndLabel.setPadding(new Insets(5, 5, 5, 5));
        ObservableList<Node> passwordsAndLabelChildrens = passwordsAndLabel.getChildren();
        passwordsAndLabelChildrens.addAll(passwordsLabel, passwordsQuantity);

        HBox spins = new HBox(5);
        spins.setPadding(new Insets(5, 5, 5, 5));
        ObservableList<Node> spinsChildren = spins.getChildren();
        spinsChildren.addAll(symbolsAndLabel, passwordsAndLabel);

        HBox upperAndLower = new HBox(5);
        upperAndLower.setPadding(new Insets(5, 5, 5, 5));
        ObservableList<Node> upperAndLowerChildren = upperAndLower.getChildren();
        upperAndLowerChildren.addAll(uppersFlag, lowersFlag);

        HBox digitsAndPunctuations = new HBox(5);
        digitsAndPunctuations.setPadding(new Insets(5, 5, 5, 5));
        ObservableList<Node> digitsAndPunctuationsChildren = digitsAndPunctuations.getChildren();
        digitsAndPunctuationsChildren.addAll(digitsFlag, punctuationsFlag);

        VBox checkBoxes = new VBox(5);
        checkBoxes.setPadding(new Insets(5, 5, 5, 5));
        ObservableList<Node> checkBoxesChildren = checkBoxes.getChildren();
        checkBoxesChildren.addAll(upperAndLower, digitsAndPunctuations);

        VBox mainLayout = new VBox(5);
        mainLayout.setPadding(new Insets(5, 5, 5, 5));
        ObservableList<Node> mainLayoutChildren = mainLayout.getChildren();
        mainLayoutChildren.addAll(checkBoxes, spins, generatePasswords, passwordsList);

        this.getChildren().add(mainLayout);
    }

    private Spinner<Integer> createSpinner(int minValue, int maxValue, int initialValue) {
        Spinner<Integer> spinner = new Spinner<>(minValue, maxValue, initialValue);
        spinner.getEditor().addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (event.getClickCount() == 2) {
                if (!spinner.editableProperty().get()) {
                    spinner.setEditable(true);
                }
            }
        });
        spinner.getEditor().addEventHandler(ScrollEvent.SCROLL, event -> {
            if (event.getDeltaY() > 0) {
                spinner.increment();
            }
            if (event.getDeltaY() < 0) {
                spinner.decrement();
            }
        });
        spinner.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("-?\\d+")) {
                spinner.getEditor().textProperty().setValue(oldValue);
            }
        });
        spinner.focusedProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue) {
                if (spinner.editableProperty().get()) {
                    spinner.setEditable(false);
                }
            }
        });
        return spinner;
    }

    private CheckBox createCheckBox(String label) {
        CheckBox checkBox = new CheckBox(label);
        checkBox.setSelected(true);

        checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> generateButtonEnabledCheck());

        return checkBox;
    }

    private void setPasswordsButtonText(Integer value) {
        if (value == 1) {
            generatePasswords.setText("Generate password");
            return;
        }
        generatePasswords.setText("Generate passwords");
    }

    private void generateButtonEnabledCheck() {
        if (!uppersFlag.selectedProperty().get() && !lowersFlag.selectedProperty().get()
                && !digitsFlag.selectedProperty().get() && !punctuationsFlag.selectedProperty().get()) {

            generatePasswords.setDisable(true);
            return;
        }
        generatePasswords.setDisable(false);
    }

    private void generatePasswords() {
        ObservableList<String> passwordsListItems = passwordsList.getItems();
        if (passwordsListItems.size() > 0) {
            passwordsListItems.clear();
        }

        PasswordGenerator passwordGenerator = new PasswordGenerator.PasswordGeneratorBuilder()
                .useUpper(uppersFlag.selectedProperty().get())
                .useLower(lowersFlag.selectedProperty().get())
                .useDigits(digitsFlag.selectedProperty().get())
                .usePunctuation(punctuationsFlag.selectedProperty().get())
                .build();

        passwordsListItems.addAll(getListOfPasswords(passwordGenerator));
    }

    private List<String> getListOfPasswords(PasswordGenerator passwordGenerator) {
        return passwordGenerator.generateFewPasswords(
                this.passwordsQuantity.getValue(),
                this.symbolsQuantity.getValue()
        );
    }

    void resizeByStage(Stage stage) {
        passwordsList.setPrefHeight(stage.heightProperty().doubleValue() - 5);
    }
}

class PasswordGenerator {
    private static final String LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String DIGITS = "0123456789";
    private static final String PUNCTUATION = "!@#$%&*()_+-=[]|,./?><";

    private static final SecureRandom secureRandom = new SecureRandom();

    private boolean useLower;
    private boolean useUpper;
    private boolean useDigits;
    private boolean usePunctuation;

    private PasswordGenerator(PasswordGeneratorBuilder builder) {
        this.useLower = builder.useLower;
        this.useUpper = builder.useUpper;
        this.useDigits = builder.useDigits;
        this.usePunctuation = builder.usePunctuation;
    }

    private String generate(int length) {
        if (length <= 0) {
            return "";
        }

        // Variables.
        StringBuilder password = new StringBuilder(length);

        // Collect the categories to use.
        List<String> charCategories = new ArrayList<>(4);
        if (useLower) {
            charCategories.add(LOWER);
        }
        if (useUpper) {
            charCategories.add(UPPER);
        }
        if (useDigits) {
            charCategories.add(DIGITS);
        }
        if (usePunctuation) {
            charCategories.add(PUNCTUATION);
        }

        // Build the password.
        for (int i = 0; i < length; i++) {
            String charCategory = charCategories.get(secureRandom.nextInt(charCategories.size()));
            int position = secureRandom.nextInt(charCategory.length());
            password.append(charCategory.charAt(position));
        }
        return new String(password);
    }

    List<String> generateFewPasswords(int passwordsQuantity, int length) {
        List<String> result = new ArrayList<>(passwordsQuantity);
        for (; passwordsQuantity > 0; --passwordsQuantity) {
            result.add(generate(length));
        }
        return result;
    }

    static class PasswordGeneratorBuilder {

        private boolean useLower;
        private boolean useUpper;
        private boolean useDigits;
        private boolean usePunctuation;

        PasswordGeneratorBuilder() {
            this.useLower = false;
            this.useUpper = false;
            this.useDigits = false;
            this.usePunctuation = false;
        }

        PasswordGeneratorBuilder useLower(boolean useLower) {
            this.useLower = useLower;
            return this;
        }

        PasswordGeneratorBuilder useUpper(boolean useUpper) {
            this.useUpper = useUpper;
            return this;
        }

        PasswordGeneratorBuilder useDigits(boolean useDigits) {
            this.useDigits = useDigits;
            return this;
        }

        PasswordGeneratorBuilder usePunctuation(boolean usePunctuation) {
            this.usePunctuation = usePunctuation;
            return this;
        }

        PasswordGenerator build() {
            return new PasswordGenerator(this);
        }
    }
}