package com.susp.passwords.generator;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        stage.setTitle("Secure passwords generator");

        PasswordWindow passwordWindow = new PasswordWindow();
        stage.setScene(new Scene(passwordWindow));
        stage.getScene().setFill(null);
        stage.heightProperty().addListener((observable, oldValue, newValue) -> passwordWindow.resizeByStage(stage));

        stage.show();
    }
}